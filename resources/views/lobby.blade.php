<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery.toast.css">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>

    <body>
        <div class="container">
            <section id="content">
                <div class="loading">Loading....</div>
                <div class="login_join_div" style="display: block;padding-bottom: 25px;">
                    <form action="" id="login_form">

                        <div class="row">
                            <div class="col-md-12" style="">
                                <input type="text" placeholder="Enter Name" id="nameText" name="nameText">
                            </div>
                        </div>
                        <div class="row d-none">
                            <div class="col-md-12" style="">
                                <input type="text" value="{{$id}}" placeholder="Enter Room ID" id="roomName" name="roomName">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="">
                                <input type="submit" value="Join" class="btn btn-primary " id="joinRoom">
                            </div>
                        </div>
                        <span id="message" style="color: red"></span>
                        <br><br><br>
                    </form><!-- form -->
                </div>
            </section>

            <!-- content -->
        </div><!-- container -->
    </body>
    <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery.toast.js"></script>
    <script src="/js/index.js"></script>
    <script src="/js/room.js"></script>
</body>

</html>
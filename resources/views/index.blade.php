<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.toast.css">
    <link rel="stylesheet" href="css/style.min.css">
</head>

<body>
<body>
<div class="container">
    <section>

        <div class="login_join_div" style="display: block;padding-bottom: 25px;">
            <form action="" id="login_form">               
                <div class="row text-center">
                     <div class="col-md-12  text-white " id="room-link"></div>
                    <div class="col-md-12 mt-5" >
                         <button type="button" class="btn btn-primary" id="create_room" >Create Sharable Link</a>
                    </div>
                </div>
    
            </form><!-- form -->
        </div>
    </section>
  
    <!-- content -->
</div><!-- container -->
</body>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery.toast.js"></script>
<script src="js/index.js"></script>
<script src="js/room.js"></script>
</body>
</html>
